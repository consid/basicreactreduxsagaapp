import {
  getProductFamiliesRoutine,
  setSettingAction
} from "../actions/productFamilyActions";

const initialState = {
  families: null,
  loading: false,
  loaded: false,
  error: null
};

const settingsReducer = (state = initialState, action) => {
  switch (action.type) {
    case setSettingAction: {
      console.log("setSettingAction from reducer");
      console.log("action", action);
      return state;
    }
    case getProductFamiliesRoutine.REQUEST: {
      const newState = {
        ...state,
        families: null,
        loading: true,
        loaded: false
      };
      return newState;
    }
    case getProductFamiliesRoutine.SUCCESS: {
      const newState = { ...state, families: action.payload.families };
      return newState;
    }
    case getProductFamiliesRoutine.FAILURE: {
      const newState = { ...state, error: action.payload.error };
      return newState;
    }
    case getProductFamiliesRoutine.FULFILL: {
      const newState = {
        ...state,
        loading: false,
        loaded: state.families != null
      };
      return newState;
    }
    default:
      break;
  }
  return state;
};

export default settingsReducer;
