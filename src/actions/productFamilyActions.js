import { createRoutine } from "redux-saga-routines";
import { createAction } from "redux-actions";

export const getProductFamiliesRoutine = createRoutine("GET_PRODUCT_FAMILIES");
export const setSettingAction = createAction("SET_SETTING_EXAMPLE_ACTION");

export const getProductFamiliesActionCreator = (id, name) => {
  return getProductFamiliesRoutine({ id, name });
};
