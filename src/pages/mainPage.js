import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import {
  getProductFamiliesRoutine,
  setSettingAction,
  getProductFamiliesActionCreator
} from "./../actions/productFamilyActions";

class mainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.loadFamilies = this.loadFamilies.bind(this);
  }
  loadFamilies() {
    this.props.getProductFamilies();
  }
  render() {
    const families = this.props.productFamilies.map(f => (
      <li key={f.id}>
        {f.id}, {f.name}
      </li>
    ));
    return (
      <div>
        <h1>Main</h1>
        <div>{this.props.productFamiliesLoading ? "Loading" : ""}</div>
        <div>
          <ol>{families}</ol>
        </div>
        <div>
          <button onClick={this.loadFamilies}>Load families</button>
        </div>
        <div>
          <button onClick={this.props.setSettingAction}>Set settings</button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    productFamilies: state.productFamilyReducer.families || [],
    productFamiliesLoading: state.productFamilyReducer.loading
  };
};

const mapDispatchToProps = dispatch => {
  const mySecretId = 123;
  return {
    getProductFamilies: () =>
      dispatch(getProductFamiliesActionCreator(mySecretId, "Original Alu")),
    setSettingAction: () => dispatch(setSettingAction({ fruit: "banana" }))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(mainPage);

mainPage.propTypes = {
  productFamiliesLoading: PropTypes.number.isRequired
};
