import React from "react";
import { Provider } from "react-redux";
import Routes from "./utils/Routes";
import { ConnectedRouter } from "connected-react-router";
import history from "./utils/history";
import store from "./utils/store";
import "./App.scss";

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <div className="page">
            <header>Header</header>
            <Routes loggedIn={this.props.loggedIn} />
          </div>
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
