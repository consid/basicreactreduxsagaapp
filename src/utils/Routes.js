import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import mainPage from "./../pages/mainPage";
import loginPage from "./../pages/loginPage";

class Routes extends React.Component {
  render() {
    let routes = null;
    if (this.props.loggedIn === false) {
      routes = <Route component={loginPage} />;
    } else {
      routes = (
        <Switch>
          <Route exact path="/" component={mainPage} />
          <Route exact path="/login" component={loginPage} />
        </Switch>
      );
    }
    return (
      <div className="page__routes">
        <nav>Nav here</nav>
        <div className="page__routes-routes">{routes}</div>
      </div>
    );
  }
}

export default Routes;
