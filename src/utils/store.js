import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import { connectRouter, routerMiddleware } from "connected-react-router";

import saga from "./../sagas";
import productFamilyReducer from "./../reducers/productFamilyReducer";

import history from "./history";

const sagaMiddleware = createSagaMiddleware();

const dev = process.env.NODE_ENV === "development";
const composeEnhancers =
  dev === true &&
  typeof window === "object" &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      })
    : compose;

export default createStore(
  combineReducers({
    productFamilyReducer,
    // Lägg till nya reducers här under
    router: connectRouter(history)
  }),
  composeEnhancers(
    applyMiddleware(
      sagaMiddleware,
      routerMiddleware(history)
      // ... other middlewares ...
    )
  )
);

sagaMiddleware.run(saga);
