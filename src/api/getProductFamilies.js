import axios from "axios";

export const getProductFamilies = () => {
  return axios.get("/api/productFamilies");
};
