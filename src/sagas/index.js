import { all, call } from "redux-saga/effects";
import { routinePromiseWatcherSaga } from "redux-saga-routines";

import productFamilySaga from "./productFamilySaga";

export default function* rootSaga() {
  yield all([
    call(productFamilySaga),
    // Lägg till nya sagor här under
    call(routinePromiseWatcherSaga)
  ]);
}
