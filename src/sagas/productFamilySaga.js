import { takeEvery, all, put, call, delay } from "redux-saga/effects";

import { getProductFamiliesRoutine } from "../actions/productFamilyActions";
import { getProductFamilies } from "./../api/getProductFamilies";

export default function* watchActions() {
  yield all([
    takeEvery(getProductFamiliesRoutine.TRIGGER, getProductFamiliesTrigger)
  ]);
}

const staticFamilies = [
  {
    id: "AFH",
    name: "Original Alu"
  },
  {
    id: "EFH",
    name: "Original Trä"
  },
  {
    id: "LOR",
    name: "Lorem ipsum"
  }
];

function* getProductFamiliesTrigger(action) {
  try {
    yield put(getProductFamiliesRoutine.request());
    const { payload } = action;
    console.log("payload", payload);
    // const result = yield axios.get('/api/productFamily');
    // const result = yield call(getProductFamilies, 123123, 123123);// I detta fallet tar getProductFamilies två parametrar
    const result = yield delay(2000, { data: { families: staticFamilies } });
    yield put(
      getProductFamiliesRoutine.success({
        families: result.data.families
      })
    );
  } catch (error) {
    yield put(
      getProductFamiliesRoutine.failure({
        success: false,
        error: error.message
      })
    );
  }
  yield put(getProductFamiliesRoutine.fulfill());
}
